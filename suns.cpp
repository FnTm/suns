/*
 * File:   suns.cpp
 * Author: Janis
 *
 * Created on tre?diena, 2011, 28 decembris, 15:16
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string.h>
#include <fstream>
#define SIZE 51

using namespace std;

int strcmp(const char* p1, const char* p2) {
    int dist = 0;

    while (!dist && *p1 && *p2)

        dist = (*p2++) - (*p1++);
    if (dist > 0)
        return (-1);
    else if (dist < 0)
        return (1);

    return (0);
}

struct List {

    struct item {
        int id;
        char* data;
        bool used;
        item* next;

        item() {
            id = 0;
            used = false;
        }
    };
    item* first;
    item* last;
    item* current;
    int count;

    void SetUsed(bool value) {
        for (item* p = first; p != NULL; p = p->next) {
            p->used = value;
        }
    }

    List() {
        first = NULL;
        last = NULL;
        current = NULL;
        count = 0;
    };

    bool IsUsed() {
        if (current != NULL) {
            return current->used;
        } else {
            return true;
        }
    }

    void IsUsed(bool value) {
        if (current != NULL) {
            current->used = value;
        }
    }

    void Add(char* data) {
        count++;
        item* p = new item;
        char* c = new char[strlen(data) + 1];
        memcpy(c, data, strlen(data) + 1);
        p->data = c;
        p->next = NULL;
        if (first == NULL) {
            last = p;
            first = p;
        } else {
            last->next = p;
            last = last->next;
        }
    }

    int Count() {
        return count;
    }

    void First() {
        current = first;
    }

    void Next() {
        if (current != NULL) {
            current = current->next;
        }
    }

    bool Eof() {
        if (current == NULL) {
            return true;
        } else {
            return false;
        }
    }

    char* GetData() {
        if (current != NULL) {
            return current->data;
        } else {
            return " ";
        }
    }

    bool FindByData(List list) {
        for (item* p = list.first; p != NULL; p = p->next) {
            if (strcmp(current->data, p->data) == 0) {
                return true;
            }
        }
        return false;
    }

    void SetId(int value) {
        if (last != NULL) {
            last->id = value;
        }
    }

    void SetIdToCurrent(int value) {
        if (current != NULL) {
            current->id = value;
        }
    }

    int GetId() {
        if (current != NULL) {
            return current->id;
        } else {
            return 0;
        }
    }
};

class Suns;

class Suns {
public:
    int id;
    Suns *father;
    Suns *mother;
    int alive;
    Suns *next;
    List *name;
    int nameLength;

    Suns(int id) {
        this->id = id;
        this->alive = 1;
        this->father = this->mother = this->next = NULL;
        this->name = NULL;
        this->nameLength=0;
    }
    Suns() {
        this->id = 0;
        this->alive = 1;
        this->father = this->mother = this->next = NULL;
        this->name = NULL;
        this->nameLength=0;
    }

    void died() {
        this->alive = 0;
    }

    void getFullName(fstream &fout) {

        this->name->First();
        List *p = this->name;
        fout << "\"";
        while (!p->Eof()) {
            fout << p->GetData();
            //cout << p->GetData();
            p->Next();
            if (!p->Eof()) {
                fout << " ";
            }
        }
        fout << "\"" << endl;

    }
    void SetFullName (List* momNames, List* dadNames) {
		if (momNames == NULL) {
			momNames = new List;
		}
		if (dadNames == NULL) {
			dadNames = new List;
		}
		momNames->First();
		dadNames->First();
		List names;
		this->name->First();
		names.Add(this->name->GetData());
		bool nameFound;
		while ((!momNames->Eof()) || (!dadNames->Eof())) {
			nameFound = false;
			if (!momNames->Eof()) {	
				while ((!momNames->Eof()) && (!nameFound)) {
					if (!momNames->IsUsed()) {
						if (!momNames->FindByData(names)) {
							names.Add(momNames->GetData());
							momNames->IsUsed(true);
							nameFound = true;
						}
					}
					momNames->Next();
				}
			}
			nameFound = false;
			if (!dadNames->Eof()) {
				while ((!dadNames->Eof()) && (!nameFound)) {
					if (!dadNames->IsUsed()) {
						if (!dadNames->FindByData(names)) {
							names.Add(dadNames->GetData());
							dadNames->IsUsed(true);
							nameFound = true;
						}					
					}
					dadNames->Next();
				}
			}
		}
		names.First();
		names.Next();
		while (!names.Eof()) {
			if ((this->nameLength + strlen(names.GetData())) <= 50) {
				this->nameLength += strlen(names.GetData()) + 1;
				this->name->Add(names.GetData());
			}
			names.Next();
		}
		momNames->SetUsed(false);
		dadNames->SetUsed(false);
	}
   List *getFullNameList(){
       return this->name;
    }
   void SetName(char* value) {
		if (this->name == NULL) {
			name = new List;
			name->Add(value);
			this->nameLength = strlen(value);
		}
	}

};

class Suni {
    ifstream fin;
    fstream fout;
    char* input;
    Suns *root, *last;

    /*Suns *findByName(char* name) {
        if (this->root == NULL) {
            return NULL;
        }

        for (Suns *p = this->root; p != NULL; p = p->next) {
            if (strcmp(p->name->i, name) == 0) {

                return p;
            }
        }
        return NULL;

    }
     */
    Suns *findById(int id) {
        if (this->root == NULL) {
            return NULL;
        }

        for (Suns *p = this->root; p != NULL; p = p->next) {
            if (p->id == id) {

                return p;
            }
        }
        return NULL;

    }
    void addDog(Suns *sunc) {
        if (this->root == NULL) {
            this->root = sunc;
            this->last = this->root;
        } else {
            this->last->next = sunc;
            this->last = sunc;
        }

    }
public:

    Suni() {
        fin.open("suns.in");
        fout.open("suns.out", ios::out);
        input = "";
        this->root = NULL;
        this->last = NULL;
        this->readInput();
    }

    void readInput() {

        char c;
        do {
            this->fin >> c;
            switch (c) {
                case 'Q':
                    this->Quit();
                    break;
                case 'R':
                    this->Register();
                    break;
                case 'D':
                    this->Died();
                    break;
                case 'F':
                    this->Find(this->fout);
                    break;
                case 'I':
                    this->FindById();
                    break;
                case 'N':
                    this->NewSuns();
                    break;
            }
            //cout << c << endl;
        } while (this->fin.is_open() && this->fin.good());
        //this->printDogs();
    }

    void Register() {
        int dogId;
        this->fin >> dogId;
        if (this->findById(dogId) == NULL) {
            Suns* sunc = new Suns(dogId);
            sunc->name = this->GetListOfNames();
            this->addDog(sunc);
            this->printOkToFile();
        } else {
            this->printNotOkToFile();
            char* c = new char[60]; //Ielasa atlikusho rindu
            this->fin.getline(c, 60);
        }

    }

    void Quit() {
        this->printQuitToFile();
        this->fin.close();
    }
void Find(fstream &fout) { 
		List* DogIds;
		DogIds = this->GetListOfDogs(this->GetString());
               
		if(fout){
                    fout<< DogIds->Count();
                     //cout << DogIds->Count();
                }
                else{
                   // cout <<"nav atveerts";
                }
		int lmin = 999999;
		for (int i = 0; i != DogIds->Count(); i++) {
			int min = 999999;

			DogIds->First();
			for (int i = 0; i != DogIds->Count(); i++) {
				if (lmin == DogIds->GetId()) {
					DogIds->SetIdToCurrent(999999);
				}
				if (min > DogIds->GetId()) {
					min = DogIds->GetId();
				}
				DogIds->Next();				
			}
			lmin = min;
			this->fout << " " << min;
		}
		this->fout << '\n';
	}

List* GetListOfDogs(char* name) {
		List* DogIds;
		DogIds = new List;
		for (Suns* p = this->root; p != NULL; p = p->next) {
			List* saraksts = p->getFullNameList();
			saraksts->First ();
			do {	
				if (strcmp(saraksts->GetData(), name) == 0) {
					DogIds->Add(name);
					DogIds->SetId(p->id);
					break;
				}
				saraksts->Next();
			} while (!saraksts->Eof());
		}
		return DogIds;
	}
    
void Died() {

        int dogId;
        this->fin >> dogId;
        //cout << dogId << endl;
        Suns* sunc = this->findById(dogId);
        /** @todo j?pievieno izvade ja suns ir miris*/
        if (sunc != NULL) {
            //sunc->getFullName(this->fout);
            sunc->died();
            this->printOkToFile();
        } else {
            this->printNotOkToFile();
        }


    }
void FindById() {
        int dogId;
        this->fin >> dogId;
        //cout << dogId << endl;
        Suns* sunc = this->findById(dogId);

        if (sunc != NULL) {
            if(sunc->alive==0){
                this->printDiedToFile();
            }
            sunc->getFullName(this->fout);
        } else {
            this->printNotOkToFile();
        }

    }
char* GetSunsName() {
		char* string = new char[52];
		char c;
		int i;
		this->fin >> c;
		for (i = 0; ((c != ' ') && (c != '\n')) ; this->fin.get(c), i++) {
			string[i] = c;
		}
		string[i] = '\0';
		return string;
	}
   void NewSuns() { 
		int mId;
		int dId;
		this->fin >> mId;
		this->fin >> dId;
		int NewDogsCount;
		this->fin >> NewDogsCount;
		for (int i = 0; i < NewDogsCount; i++) {
			Suns* p = new Suns;
			if (last != NULL) {
				if (last->id < 100000) {
				p->id = 100000;
				} else {
					p->id = last->id + 1;
				}
			} else {
				p->id = 100000;
			}

			p->SetName (this->GetSunsName());
			Suns* mom = this->findById(mId);
			Suns* dad = this->findById(dId);
			if ((mom != NULL) && (dad != NULL)) {
				p->SetFullName (mom->getFullNameList(), dad->getFullNameList());
			} else if ((mom == NULL) && (mId == 0) && (dad != NULL)) {
				p->SetFullName (NULL, dad->getFullNameList());
			} else if ((mom != NULL) && (dad == NULL) && (dId == 0)) {
				p->SetFullName (mom->getFullNameList(), NULL);
			} else if ((mom == NULL) && (dad == NULL) && (mId == 0) && (dId == 0)) {
				//
			} else {
				this->printNotOkToFile();
				break;
			}
			p->next = NULL;		
			if (this->root == NULL) {
				last = p;
				this->root = p;			
			} else {
				last->next = p;
				last = last->next;
			}
			this->fout << p->id << " ";
			p->getFullName(this->fout);
		}
	}

    void printNotOkToFile() {
        this->fout << "no" << endl;
    }

    void printOkToFile() {
        this->fout << "ok" << endl;
    }
     void printQuitToFile() {
        this->fout << "quit" << endl;
    }
     void printDiedToFile(){
         this->fout << "dead - ";
     }

    List* GetListOfNames() {
        char c = ' ';
        List* list = new List;
        while (c != '\n' && this->fin.good()) {
            this->fin.get(c);
            char* tempName;
            char maxname[53];
            if ((c != '"') && (c != ' ') && (c != '\n')) {
                int i;
                for (i = 0; ((c != ' ') && (c != '"') && (c != '\n')); i++) {
                    maxname[i] = c;
                    this->fin.get(c);
                }
                maxname[i] = '\0';
                tempName = maxname;
                list->Add(tempName);
            }
        }
        return list;
    }

    void printDogs() {
        for (Suns *p = this->root; p != NULL; p = p->next) {
            // cout << p->id << " ";
            p->getFullName(this->fout);
            // cout << " " << p->alive << endl;
        }
    }
    char* GetString() {
		char* string = new char[50];
		char c;
		int i;
		this->fin >> c;
		for (i = 0 ; c != '\n' ; this->fin.get(c), i++) {
			if (c != '"') {
				string[i] = c;
			} else {
				i--;
			}
		}
		string[i] = '\0';
		return string;
	}

};

int main() {
    new Suni();


    return 0;
}

